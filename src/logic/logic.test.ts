/// <reference path="../../node_modules/@types/jest/index.d.ts" />
import { DbStore } from "../store/dbstore";
import { confDefault } from "../conf";
import { Logic } from "./logic";
import { UserLogic } from "./userlogic";

describe("Logic", () => {

    let logic: Logic;

    beforeAll(() => {
        const dbStore = new DbStore(confDefault.db);
        const userLogic = new UserLogic(dbStore);
        logic = new Logic(dbStore, null, userLogic, null);
    });

    afterAll(async () => {
        await logic.close();
    });

    describe("User", () => {

        test("User find", async () => {
            const u = await logic.user().find();
            expect(u.length).toBeGreaterThan(0);
        });

    });

});
