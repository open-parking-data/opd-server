import * as log4js from "log4js";
import { basename, extname } from "path";
import { DbStore } from "../store/dbstore";
import { OpdData } from "../model/opddata";
import { mongoIdToStr } from "../utils/mongoutil";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

export interface Paging {
    offset: number;
    limit: number;
    total?: number;
}

export class OpdDataLogic {

    private static _collection = "opddata";

    private _dbStore: DbStore;

    constructor(db: DbStore) {
        this._dbStore = db;
    }

    async removeAll(): Promise<void> {
        const db = await this._dbStore.connect();
        return new Promise<void>((resolve, reject) => {
            db.collection<OpdData>(OpdDataLogic._collection)
                .deleteMany({}, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }

    async insert(user: OpdData): Promise<string> {
        const db = await this._dbStore.connect();
        return new Promise<string>((resolve, reject) => {
            db.collection<OpdData>(OpdDataLogic._collection)
                .insertOne(user, (err, res) => {
                    if (err) {
                        log.error(err);
                        reject(err);
                    } else {
                        log.debug("OpdData inserted:", res.ops[0]);
                        resolve(res.ops[0]._id);
                    }
                });
        });
    }

    async find(paging: Paging = { offset: 0, limit: 10 }): Promise<OpdData[]> {
        const db = await this._dbStore.connect();
        return new Promise<OpdData[]>((resolve, reject) => {
            db.collection<OpdData>(OpdDataLogic._collection)
                .find<OpdData>()
                .sort({ login: 1 })
                .skip(paging.offset)
                .limit(paging.limit)
                .toArray((err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res.map(r => mongoIdToStr(r)));
                    }
                });
        });
    }

    async findById(id: string): Promise<OpdData> {
        const db = await this._dbStore.connect();
        return new Promise<OpdData>((resolve, reject) => {
            db.collection<OpdData>(OpdDataLogic._collection)
                .findOne<OpdData>({ id: id }, (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(mongoIdToStr(res));
                    }
                });
        });
    }

}
