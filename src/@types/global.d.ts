import { Logic } from "../logic/logic";
import { User } from "../model/user";
import { Conf } from "../conf";

declare global {
    namespace Express {
        interface Application {
            conf: Conf;
            logic: Logic;
        }
    }
    namespace Express {
        interface Request {
            absBaseUrl: string;
            absUrl: string;
            auth:  { user: string, password: string };
            user: User | undefined;
        }
    }
}
