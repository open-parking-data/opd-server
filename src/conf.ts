export const confDefault = {
    log4js: {
        appenders: { out: { type: "stdout" } },
        categories: { default: { appenders: ["out"], level: "trace" } }
    },
    server: {
        host: "localhost",
        port: 8080
    },
    mailer: {
        name: "opd",
        address: "opd@gmail.com",
        password: "opd-pwd"
    },
    db: {
        url: "mongodb://root:opds@localhost:27017/admin"
    },
    dbdump: "data/db-dump",
    files: "data/files"
};

export type Conf = typeof confDefault;
