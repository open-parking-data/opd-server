
export interface OpdData {
    id: string;
    position: {
        lat: number;
        lon: number;
    };
    // position: [number, number];
    occupied: boolean;
    price: number;
    meta: {
        address: string;
        value: number;
    };
}
