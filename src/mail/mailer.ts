import * as log4js from "log4js";
import { basename, extname } from "path";
import * as nodemailer from "nodemailer";
import { SentMessageInfo } from "nodemailer/lib/smtp-connection";

const log = log4js.getLogger(basename(__filename, extname(__filename)));
// log.level = "debug";

// Google as free SMTP https://kinsta.com/knowledgebase/free-smtp-server/
// Enable Less secure app access https://myaccount.google.com/lesssecureapps
//
// {
//     service: "gmail",
//     // secure: true,
//     auth: {
//         user: "pr.rybar@gmail.com",
//         pass: "xxxxxxxxxxx"
//     }
// }
//
// {
//     host: "smtp.gmail.com",
//     port: 465, // SSL
//     // port: 587, // TLS
//     secure: true, // use SSL/TLS
//     auth: {
//         user: "pr.rybar@gmail.com",
//         pass: "xxxxxxxxxxxx"
//     }
// }

export interface Conf {
    name: string;
    address: string;
    password: string;
}

export interface Message {
    to: string;
    subject: string;
    text: string;
    html: string;
}

export class Mailer {

    private _conf: Conf;
    private _transport: nodemailer.Transporter;

    constructor(conf: Conf) {
        this._conf = conf;
    }

    openTransport() {
        const options = {
            service: "gmail",
            auth: {
                user: this._conf.address,
                pass: this._conf.password
            }
        };
        log.debug("transport create");
        this._transport = nodemailer.createTransport(options);
    }

    async sendMessage(message: Message): Promise<SentMessageInfo> {
        const mail = {
            from: `${this._conf.name} <${this._conf.address}>`,
            ...message
        };
        const r = await this._transport.sendMail(mail);
        log.debug("sendMail response", r);
        return r;
    }

    closeTransport() {
        log.debug("transport close");
        this._transport.close();
    }

}
