
export class Events<C = any> {

    static any = "";

    private _ctx: C;
    private _cbs: { [e: string]: Array<(data: any, ctx: C, e: string) => void> };
    private _cb: Array<(data: any, ctx: C, e: string) => void>;

    constructor(ctx?: C) {
        this._cbs = {};
        this._ctx = ctx;
    }

    emit(e: string, data?: any): this {
        if (e in this._cbs) {
            for (let i = 0, l = this._cbs[e].length; i < l; i++) {
                this._cbs[e][i](data, this._ctx, e);
            }
        }
        if (this._cb) {
            for (let i = 0, l = this._cb.length; i < l; i++) {
                this._cb[i](data, this._ctx, e);
            }
        }
        return this;
    }

    on(e: string, cb: (data: any, ctx: C, e: string) => void): this {
        if (!e) {
            if (!this._cb) {
                this._cb = [];
            }
            this._cb.push(cb);
        }
        if (!(e in this._cbs)) {
            this._cbs[e] = [];
        }
        if (this._cbs[e].indexOf(cb) === -1) {
            this._cbs[e].push(cb);
        }
        return this;
    }

    any(cb: (data: any, ctx: C, e: string) => void): this {
        this.on(Events.any, cb);
        return this;
    }

    all(es: string[], cb: (data: any, ctx: C, e: string) => void): this {
        es.forEach(e => this.on(e, cb));
        return this;
    }

    once(e: string, cb: (data: any, ctx: C, e: string) => void): this {
        const wrap = (d: any, c: C, ev: string) => {
            this.off(e, wrap);
            cb(d, c, ev);
        };
        this.on(e, wrap);
        return this;
    }

    off(e: string, cb?: (data: any, ctx: C, e: string) => void): this {
        if (!e) {
            if (cb) {
                this._cb.splice(this._cbs[e].indexOf(cb), 1);
            } else {
                this._cb.length = 0;
                delete this._cb;
            }
        }
        if (e in this._cbs) {
            if (cb) {
                this._cbs[e].splice(this._cbs[e].indexOf(cb), 1);
            } else {
                this._cbs[e].length = 0;
                delete this._cbs[e];
            }
        }
        return this;
    }

    cbs(...cbs: { [e: string]: (data: any, ctx: C, e: string) => void }[]): this {
        cbs.forEach(a =>
            Object.keys(a).forEach(k =>
                this.on(k, a[k])));
        return this;
    }

}


// const es = new Events<number>(3);

// es.any((data, ctx, e) => console.log("any:", data, ctx, e));

// es.emit("e", "eee1");
// es.on("e", (data, ctx, e) => console.log(data, ctx, e));
// es.emit("e", "eee2");
// es.off("e");
// es.emit("e", "eee3");

// es.off(Events.any);

// es.once(Events.any, (data, ctx, e) => console.log("once all:", data, ctx, e));

// es.emit("o", "ooo1");
// es.once("o", (data, ctx, e) => console.log(data, ctx, e));
// es.emit("o", "ooo2");
// es.emit("o", "ooo3");

// es.all(["e1", "e3"], (data, ctx, e) => console.log(data, ctx, e));
// es.emit("e1", "all e1");
// es.emit("e2", "all e2");
// es.emit("e3", "all e3");

// es.cbs(
//     {
//         ex1 : (data, ctx, e) => console.log("ex1-1:", data, ctx, e),
//         ex2 : (data, ctx, e) => console.log("ex2-1:", data, ctx, e)
//     },
//     {
//         ex1 : (data, ctx, e) => console.log("ex1-1:", data, ctx, e),
//         ex2 : (data, ctx, e) => console.log("ex2-2:", data, ctx, e)
//     }
// );
// es.emit("ex1", "ex1-data");
// es.emit("ex2", "ex2-data");
