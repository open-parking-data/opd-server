import * as log4js from "log4js";
import { basename, extname } from "path";
import * as nconf from "nconf";
import * as fs from "fs";
import * as path from "path";
import * as express from "express";
import * as helmet from "helmet";
import * as compression from "compression";
import * as session from "express-session";
import * as cookieParser from "cookie-parser";
import { users } from "./data/users";
import { confDefault, Conf } from "./conf";
import { objPaths } from "./prest/objpaths";
import { rootRouter } from "./router/root";
import { apiRouter } from "./router/api";
import { jserrRouter } from "./router/jserr";
import { demoRouter } from "./router/demo";
import { DbStore } from "./store/dbstore";
import { Mailer } from "./mail/mailer";
import { UserLogic } from "./logic/userlogic";
import { OpdDataLogic } from "./logic/opddatalogic";
import { Logic } from "./logic/logic";
import { errorLog, errorJsonResponse, errorPage } from "./middleware/errorhandlers";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

nconf
    .argv()
    .env({
        separator: "_",
        lowerCase: false,
        whitelist: objPaths(confDefault).map(c => c.join("_")),
        parseValues: true
    })
    .file(path.join(__dirname, "..", "conf.json"))
    .defaults(confDefault);

const conf = nconf.get() as Conf;

log4js.configure(conf.log4js);

log.info("NODE_ENV", process.env.NODE_ENV);
log.info("conf: %s", JSON.stringify(conf, null, 4));

const dbStore = new DbStore(conf.db);
const logic = new Logic(
    dbStore,
    new Mailer(conf.mailer),
    new UserLogic(dbStore),
    new OpdDataLogic(dbStore));

(async () => {
    await logic.user().removeAll();
    await Promise.all(users.map(async u => logic.user().insert(u)));

    // if (process.env.NODE_ENV === "production") {
    //     const data = await dbStore.dumpAll();
    //     const name = `backup-${new Date().toISOString()}`;
    //     const p = path.join(conf.dbdump, name);
    //     log.info("DB dump", p, " - ",
    //         Object.keys(data)
    //             .map(k => `${k}: ${data[k].length}`)
    //             .join(", "));
    //     fs.writeFileSync(p, JSON.stringify(data, null, 4));
    // } else {
    // }
})();

export const app = express();

app.conf = conf;
app.logic = logic;

app.use(log4js.connectLogger(log4js.getLogger("http"), { level: "auto" }));
app.use(helmet());
app.disable("x-powered-by");
app.use(compression());
app.set("trust proxy", 1); // trust first proxy
app.use( // https://github.com/andybiar/express-passport-redis-template/blob/master/app.js
    session({
        secret: "se-cu-re",
        name: "sessionId",
        resave: false,
        saveUninitialized: true
    })
);
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
// app.use(bodyParser.text());
app.use(cookieParser());

app.use((req, res, next) => {
    req.absBaseUrl = req.protocol + "://" + req.get("host");
    req.absUrl =  req.absBaseUrl + req.originalUrl;
    return next();
});

app.set("json spaces", 3);

app.use("/", rootRouter);
app.use("/api", apiRouter);
app.use("/jserr", jserrRouter);
app.use("/demo", demoRouter);

app.use("/assets", express.static(path.join(__dirname, "../assets")));

const clientDist = path.join(__dirname, "../../client/dist");
if (fs.existsSync(clientDist)) {
    app.use(express.static(clientDist));
} else {
    app.use(express.static(path.join(__dirname, "../public")));
}

app.use(errorLog);
app.use(errorJsonResponse);
app.use(errorPage);
