import * as log4js from "log4js";
import { basename, extname } from "path";
import { Db, Collection, MongoClient } from "mongodb";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

export interface DbStoreConf {
    url: string;
}

export class DbStore {

    private _client: MongoClient;
    private _db: Db;
    private _conf: DbStoreConf;

    // public ObjectID: (id: string) => any;
    // public ObjectID: ObjectID;

    constructor(conf?: DbStoreConf) {
        this._conf = conf;
    }

    connect(): Promise<Db> {
        return new Promise<Db>((resolve, reject) => {
            try {
                if (this._db) {
                    return resolve(this._db);
                }

                // const { url, user, password, database } = this._config;
                const { url } = this._conf;

                MongoClient.connect(url, { useNewUrlParser: true },
                    (err, client) => {
                        if (err) {
                            log.debug("DB connect failed:", err);
                            reject(err);
                        } else {
                            this._client = client;
                            const db = client.db();
                            this._db = db;
                            // this.ObjectID = ObjectID;
                            log.debug("DB connected");
                            resolve(this._db);
                        }
                    });
            } catch (e) {
                this._db = null;
                reject(e);
            }
        });
    }

    close(): Promise<void> {
        return new Promise<void>((resolve) => {
            this._client.close(false, () => resolve());
        });
    }

    async collectionNames(): Promise<string[]> {
        const db = await this.connect();
        return new Promise<string[]>((resolve, reject) => {
            db.collections((err, res) => {
                if (err) {
                    reject(err);
                } else {
                    const names = res.map(c => c.collectionName);
                    resolve(names);
                }
            });
        });
    }

    async collection<U>(name: string): Promise<Collection<U>> {
        const db = await this.connect();
        return new Promise<Collection<U>>((resolve) => {
            const collection = db.collection<U>(name);
            resolve(collection);
        });
    }

    async collectionDrop(name: string): Promise<boolean> {
        const col = await this.collection(name);
        return new Promise<boolean>((resolve, reject) => {
            col.drop((err, res) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(res);
                }
            });
        });
    }

    async dump<U>(collectionName: string): Promise<U[]> {
        const db = await this.connect();
        return new Promise<U[]>((resolve, reject) => {
            db.collection(collectionName)
                .find().toArray((err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
        });
    }

    async dumpAll(): Promise<any> {
        const collections = await this.collectionNames();
        const data: any = {};
        await Promise.all(
            collections.map(async c => {
                const dump = await this.dump(c);
                data[c] = dump;
            }));
        return data;
    }

    async load<U>(collectionName: string, data: U[]): Promise<U[]> {
        data.forEach(d => delete (d as any)._id);
        const db = await this.connect();
        return new Promise<U[]>((resolve, reject) => {
            db.collection<U>(collectionName)
                .insertMany(data, (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res.ops);
                    }
                });
        });
    }

    async loadAll(data: { [k: string]: any[] }): Promise<{ [k: string]: any[] }> {
        const res: { [k: string]: any[] } = {};
        for (const k in data) {
            res[k] = await this.load(k, data[k]);
        }
        return res;
    }

    async deleteAll(collectionName: string): Promise<any> {
        const db = await this.connect();
        return new Promise<any>((resolve, reject) => {
            db.collection(collectionName)
                .deleteMany(
                    {},
                    (err, res) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(res.deletedCount);
                        }
                    }
                );
        });
    }

}
