/// <reference path="../../node_modules/@types/jest/index.d.ts" />
import { DbStore } from "./dbstore";
import { confDefault } from "../conf";

describe("DBStore", () => {

    let dbStore: DbStore;

    beforeAll(() => {
        dbStore = new DbStore(confDefault.db);
    });

    afterAll(async () => {
        await dbStore.close();
    });

    describe("dbstore core", () => {

        test("collection names", async () => {
            const res = await dbStore.load("xxx", [
                { x: "x1", y: "y1"},
                { x: "x2", y: "y2"}
            ]);
            // console.log("load", res);
            expect(res.length).toBe(2);

            const collections = await dbStore.collectionNames();
            // console.log(collections);
            expect(collections).toContain("xxx");

            await dbStore.collectionDrop("xxx");
            // const collections1 = await dbStore.collectionNames();
            // console.log(collections1);
        });

        test("dump", async () => {
            const res = await dbStore.load("xxx", [
                { x: "x1", y: "y1"},
                { x: "x2", y: "y2"}
            ]);
            expect(Object.keys(res).length).toBe(2);

            const xxx = await dbStore.dump("xxx");
            // console.log(JSON.stringify(xxx, null, 4));
            expect(xxx.length).toBe(2);

            await dbStore.collectionDrop("xxx");
            // const collections1 = await dbStore.collectionNames();
            // console.log(collections1);
        });

        test("dump all", async () => {
            const data = await dbStore.dumpAll();
            // console.log(JSON.stringify(data, null, 4));
            const collections = await dbStore.collectionNames();
            expect(Object.keys(data).length).toBe(collections.length);
        });

        test("load", async () => {
            const res = await dbStore.load("xxx", [
                { x: "x1", y: "y1"},
                { x: "x2", y: "y2"}
            ]);
            // console.log(JSON.stringify(res, null, 4));
            expect(res.length).toBe(2);

            const r = await dbStore.deleteAll("xxx");
            // console.log(JSON.stringify(r, null, 4));
            expect(r).toBe(2);

            await dbStore.collectionDrop("xxx");
            // const collections = await dbStore.collectionNames();
            // console.log(collections);
        });

        test("load all", async () => {
            const data = {
                "xxx": [
                    { x: "x1", y: "y1" },
                    { x: "x2", y: "y2" }
                ],
                "yyy": [
                    { x: "x3", y: "y3" }
                ]
            };
            const res = await dbStore.loadAll(data);
            // console.log(JSON.stringify(res, null, 4));
            expect(Object.keys(res)).toEqual(["xxx", "yyy"]);

            const rx = await dbStore.deleteAll("xxx");
            // console.log(JSON.stringify(rx, null, 4));
            expect(rx).toBe(2);

            const ry = await dbStore.deleteAll("yyy");
            // console.log(JSON.stringify(ry, null, 4));
            expect(ry).toBe(1);

            await dbStore.collectionDrop("xxx");
            await dbStore.collectionDrop("yyy");
            // const collections = await dbStore.collectionNames();
            // console.log(collections);
        });

    });

});
