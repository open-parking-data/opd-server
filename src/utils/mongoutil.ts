import { ObjectID } from "mongodb";

export function mongoIdToStr<T = any>(object: {
            id?: string | ObjectID,
            _id?: string | ObjectID,
            [k: string]: any
        }): T {
    Object.keys(object).forEach((key: any) => {
        if (!object[key]) { // if null or undefined => nothing to transform
            return;
        }
        if (key === "_id") {
            object.id = object._id.toString();
            delete object["_id"];
        }
        else if (key.endsWith("Id") && ObjectID.isValid(object[key].toString())) {
            object[key] = object[key].toString();
        }
        else if (typeof object[key] === "object") {
            mongoIdToStr(object[key]);
        } else if (Array.isArray(object[key])) {
            object[key] = object[key].forEach((element: any) => {
                if (typeof element === "object") {
                    mongoIdToStr(element);
                }
            });
        }
        else if (Array.isArray(object[key])) {
            object[key] = object[key].forEach((element: any) => {
                if (typeof element === "object") {
                    mongoIdToStr(element);
                }
            });
        }
    });
    return object as T;
}
