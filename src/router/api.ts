import * as log4js from "log4js";
import { basename, extname } from "path";
import { Router, Request, Response, NextFunction } from "express";
import { authBasic } from "../middleware/authbasic";
import { rbac } from "../middleware/rbac";
import { errorCatch } from "../middleware/errorhandlers";
import * as bodyParser from "body-parser";
import { ObjectValidator, StringValidator, NumberValidator } from "prest-lib/dist/validators";
import { OpdData } from "../model/opddata";

const log = log4js.getLogger(basename(__filename, extname(__filename)));


const jsonParser = bodyParser.json();

// router.use(authBasic);
// router.use(rbac(["admin", "user"]));

const router: Router = Router();

router.get("/",
    authBasic,
    rbac(["admin", "user"]),
    errorCatch(async (req: Request, res: Response, next) => {
        log.debug("api", req.user, req.auth);
        const opdData = await req.app.logic.opdData().find();
        res.json({ auth: req.auth, data: opdData });
    }));

// Test
// curl -X POST http://rybar:peter@localhost:8080/api \
//     -H "content-type: application/json" \
//     -d '{
//         "id": "string id",
//         "position": [2134123412.123, 1234123432.321],
//         "occupied": true,
//         "price": 1.5,
//         "meta": {
//             "address": "Kosicka 4, Bratislava",
//             "value": 0.78
//         }
//     }'

// curl -X POST http://rybar:peter@localhost:8080/api \
//     -H "content-type: application/json" \
//     -d '{
//         "id": "string id",
//         "position": { "lat": 2134123412.123, "lon": 1234123432.321 },
//         "occupied": true,
//         "price": 1.5,
//         "meta": {
//             "address": "Kosicka 4, Bratislava",
//             "value": 0.78
//         }
//     }'

router.post("/",
    authBasic,
    rbac(["admin", "user"]),
    jsonParser,
    errorCatch(async (req: Request, res: Response, next) => {
        log.debug("api", req.user, req.auth, JSON.stringify(req.body));

        const ov = new ObjectValidator<OpdData>()
            .addValidator("id", new StringValidator())
            .addValidator("position", new ObjectValidator<OpdData["position"]>()
                .addValidator("lat", new NumberValidator({ min: -90, max: 90 }))
                .addValidator("lon", new NumberValidator({ min: -180, max: 180 }))
            )
            // .addValidator("position", new ArrayValidator<OdpData["position"][0]>(new NumberValidator()))
            .addValidator("occupied", new StringValidator())
            .addValidator("price", new NumberValidator())
            .addValidator("meta", new ObjectValidator<OpdData["meta"]>()
                .addValidator("address", new StringValidator())
                .addValidator("value", new NumberValidator())
            );
        ov.validate(req.body);
        if (ov.valid) {
            log.info(ov.obj, ov.str);
            const id = await req.app.logic.opdData().insert(ov.obj);
            res.json({ data: { id, ...ov.obj } });
        } else {
            log.error(ov.err);
            res.json({ errors: ov.err });
        }
    }));

router.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    if (req.is("json")) {
        if (err instanceof SyntaxError) {
            res.status((err as any).status)
                .json({ error: `${err.name}: ${err.message}` });
        } else {
            next();
        }
    } else {
        next(err);
    }
});

export const apiRouter: Router = router;
