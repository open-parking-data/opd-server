import { Router, Request, Response } from "express";
import { jsonmls2html } from "../prest/jsonml-html";
import { JsonMLs } from "../prest/jsonml";

// const log = log4js.getLogger("root");

// import * as bodyParser from "body-parser";

// parse application/json
// const jsonParser = bodyParser.json();
// app.use(jsonParser);

// parse application/x-www-form-urlencoded
// const urlencodedParser = bodyParser.urlencoded({ extended: false });
// app.use(urlencodedParser);

// const textParser = bodyParser.text();
// app.use(textParser);

// import * as cookieParser from "cookie-parser";
// app.use(cookieParser());

// app.use(authBasic);

interface PageState {
    title: string;
    lang: string;
}

function page(state: PageState) {
    return [
        "<!DOCTYPE html>",
        ["html", { lang: state.lang },
            ["head",
                ["meta", { charset: "utf-8" }],
                ["meta", { "http-equiv": "X-UA-Compatible", content: "IE=edge,chrome=1" }],
                ["meta", { name: "viewport", content: "width=device-width, initial-scale=1.0, maximum-scale=1.0" }],
                ["meta", { name: "author", content: "Peter Rybar, pr.rybar@gmail.com" }],
                ["title", state.title],
                ["link", { rel: "icon", href: "favicon.ico", type: "image/x-icon" }],
                ["link", { rel: "stylesheet", href: "https://www.w3schools.com/w3css/4/w3.css" }],
                // ["link", { rel:"stylesheet", href:"assets/css/styles.css" }]
                ["meta", { id: "theme-color", name: "theme-color", content: "#37474F" }],
                ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
                ["link", { rel: "manifest", href: "manifest.json" }]
            ],
            ["body",
                ["div.w3-container",
                    ["h1", "Open Parking Data"],
                    ["p",
                        "Project: ",
                        ["a",
                            { href: "https://gitlab.com/open-parking-data/opd-server" },
                            "https://gitlab.com/open-parking-data/opd-server"]
                    ]
                ]
            ]
        ]
    ];
}


function renderJsonML(res: Response, jsonmls: JsonMLs) {
    res.set("Content-Type", "text/html");
    // res.send(jsonmls2htmls(jsonmls, false).join(""));
    jsonmls2html(jsonmls, html => res.write(html), false);
    res.end();
}

const router: Router = Router();

router.get("/",
    (req: Request, res: Response) => {
        renderJsonML(res, page({
            title: "Project",
            lang: "en"
        }));
    });

export const rootRouter: Router = router;
