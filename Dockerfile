# docker build -t opd-server .
# docker images
# docker ps
# docker run -it --rm -p 8080:8080 opd-server
# docker system prune -a

#ARG buildno
#ARG password
#RUN echo "Build number: $buildno"
#RUN script-requiring-password.sh "$password"

# Version of node available from the Docker Hub
FROM node:10

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json /app/

# If you are building your code for production
RUN npm install --only=production

# Bundle app source
COPY . /app

# App binds to port 8080 so use the EXPOSE
EXPOSE 8080

# Run the image as a non-root user
RUN adduser --disabled-login --gecos '' appuser
USER appuser

# Define environment variable
# ENV NODE_ENV production

# CMD [ "npm", "run", "server:cluster" ]
CMD npm start
